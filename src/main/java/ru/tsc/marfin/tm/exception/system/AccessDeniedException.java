package ru.tsc.marfin.tm.exception.system;

import ru.tsc.marfin.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}
